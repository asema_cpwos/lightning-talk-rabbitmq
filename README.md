# Lightning talk - rabbitmq
### Installation:
- Install docker for Mac https://www.docker.com/docker-mac
- Run:
```sh
$ git clone ssh://git@stash.cpwonlinesolutions.com:2222/~asema/lightning-talk-rabbitmq.git
```
`composer install` in both `website` and `symfony/code`
```sh
$ docker-compose up -d
```

### Access:
| Application | URL |
| ------ | ------ |
| Website | http://localhost:5000/ |
| Symfony | http://localhost:5001/ |
| RabbitMQ | http://localhost:15672/ (username: guest / password: guest) |

