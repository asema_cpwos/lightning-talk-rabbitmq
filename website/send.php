<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/data.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();

$channel->queue_declare(
    'carrots_queue',    //queue - Queue names may be up to 255 bytes of UTF-8 characters
    false,              //passive - can use this to check whether an exchange exists without modifying the server state
    false,              //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
    false,              //exclusive - used by only one connection and the queue will be deleted when that connection closes
    false               //auto delete - queue is deleted when last consumer unsubscribes
);

echo 'Sending data to queue';
for ($i=0; $i<200000; $i++) {
	$msg = new AMQPMessage(json_encode($data['item-xs']));
	$channel->basic_publish($msg, '', 'carrots_queue');
	echo '.';
}


$channel->close();
$connection->close();
