<?php

$data = [
    'item-xs' => 'x-small text',
    'item-s' => 'I am a small text ready to be tested',
    'item-m' => 'I am a medium text - Several RabbitMQ servers on a local network can be clustered together, forming a single logical broker.',
    'item-l' => 'large text - Messages are routed through exchanges before arriving at queues. RabbitMQ features several built-in exchange types for typical routing logic. For more complex routing you can bind exchanges together or even write your own exchange type as a plugin.',
    'item-xl' => 'I am a huge text and here is a long paragraph to fill the space: What can RabbitMQ do for you?' .
        'Messaging enables software applications to connect and scale. Applications can connect to each other, as components of a larger application, or to user devices and data. Messaging is asynchronous, decoupling applications by separating sending and receiving data.' .
        'You may be thinking of data delivery, non-blocking operations or push notifications. Or you want to use publish / subscribe, asynchronous processing, or work queues. All these are patterns, and they form part of messaging.' .
        'RabbitMQ is a messaging broker - an intermediary for messaging. It gives your applications a common platform to send and receive messages, and your messages a safe place to live until received.'
];